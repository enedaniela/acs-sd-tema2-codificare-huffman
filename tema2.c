#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#define MAX_Q_SIZE 5000005

//Structura utila pentru citire
typedef struct 
{
	uint32_t asc;
	unsigned char c;
	float p;	
}readLetter;

//Structura datelor
typedef struct {
	unsigned char c;
	float p;
}letter;

//Structura unui nod din arbore
typedef struct TagHuffmanNode {
	letter data;
	int *codif;
	int16_t number;
	uint32_t nivel;
	struct TagHuffmanNode *parrent;
	struct TagHuffmanNode *fiu_stanga;
	struct TagHuffmanNode *fiu_dreapta;
} __attribute__((__packed__)) TagHuffmanNode;

//Structura cozii cu prioritati
struct node
{
    float priority;
    TagHuffmanNode *info;
    struct node *link;
}*front=NULL;

//Structura pentru realizarea decompresiei
typedef struct Huffman{
	unsigned char c;
	int16_t left;
	int16_t right;
} __attribute__((__packed__)) Huffman;

TagHuffmanNode *eSum = NULL;
TagHuffmanNode *primul_nod = NULL;
uint32_t count = 0;
uint16_t numar_noduri = 0;

//Functii pentru parcurgerea in latime
//----------------------------------------------------------------------------
TagHuffmanNode** createQueue(uint32_t *front, uint32_t *rear)
{
  	TagHuffmanNode **queue =
   		(TagHuffmanNode**)malloc(sizeof(TagHuffmanNode*)*MAX_Q_SIZE); 
 
  	*front = *rear = 0;
  	return queue;
}
 
void enQueue(TagHuffmanNode **queue, uint32_t *rear, TagHuffmanNode *new_node)
{
  	queue[*rear] = new_node;
  	(*rear)++;
}    
 
TagHuffmanNode *deQueue(TagHuffmanNode **queue, uint32_t *front)
{
  	(*front)++;
  	return queue[*front - 1];
} 

/*Functie ce numeroteaza nodurile, parcurgandu-le in latime*/
void BuildLevelOrder(TagHuffmanNode *root)
{
  	uint32_t rear=0, front=0;
  	TagHuffmanNode **queue = createQueue(&front, &rear); 
  	TagHuffmanNode *temp_node = root;

 	numar_noduri=0;
  	while(temp_node!=NULL)
  	{
  		/*Numerotez nodurile din arbore*/
  		temp_node->number = numar_noduri;
  		numar_noduri++;
 
   	 	/*Pun in coada fiul stang */
    	if(temp_node->fiu_stanga)
      		enQueue(queue, &rear, temp_node->fiu_stanga);
 
    	/*Pun in coada fiul drept */
   		if(temp_node->fiu_dreapta)
      		enQueue(queue, &rear, temp_node->fiu_dreapta);
 
    	/*Scot din coada*/
    	temp_node = deQueue(queue, &front);
  	}

}   

/*Functie ce scrie nodurile din arbore in fisierul binar*/
void printTagHuffmanNode (TagHuffmanNode* node, FILE * fp)
{
	int16_t x = -1;
	/*Scriu campul corespunzator caracterului din structura TagHuffmanNode*/
	fwrite(&node->data.c, sizeof(unsigned char),1, fp);
	/*Daca fiul stang nu este o frunza, scriem indexul*/
	if(node->fiu_stanga!=NULL)	
	{
		fwrite(&node->fiu_stanga->number, sizeof(int16_t),1, fp);
	}
	/*In caz contrar scriem variabila x, initializata cu -1*/
	else
	{
		fwrite(&x,sizeof(int16_t),1,fp);
	}
	/*Aceeasi abordare ca pentru fiul stang se aplica si fiului drept*/
	if(node->fiu_dreapta!=NULL)
	{
		fwrite(&node->fiu_dreapta->number, sizeof(int16_t),1, fp);
	}
	else
	{
		fwrite(&x,sizeof(int16_t),1,fp);
	}		
	
}

/*Functie ce parcurge arborele in latime*/
void printLevelOrder(TagHuffmanNode* root,FILE *fp)
{
  	uint32_t rear, front;
  	TagHuffmanNode **queue = createQueue(&front, &rear); 
  	TagHuffmanNode *temp_node = root;

  	while(temp_node!=NULL)
  	{
  		/*Scriem fiecare nod in fisierul binar*/
    	printTagHuffmanNode(temp_node,fp);
 
    	/*Adaug in coadafiul stang */
    	if(temp_node->fiu_stanga)
      	enQueue(queue, &rear, temp_node->fiu_stanga);
 
    	/*Adaug in coada fiul drept*/
    	if(temp_node->fiu_dreapta)
      	enQueue(queue, &rear, temp_node->fiu_dreapta);
 
    	/*Scot din coada*/
    	temp_node = deQueue(queue, &front);
  	}
}   
//---------------------------------------------------------------------------

/*Functie de adaugare noduri in arbore*/
TagHuffmanNode * ins ( TagHuffmanNode * a, TagHuffmanNode * b)
{
	TagHuffmanNode * tata;

	tata = (TagHuffmanNode *)malloc(sizeof(TagHuffmanNode));
	/*Nodul parinte va avea probabilitatea egala cu suma probabilitatilor*/
	tata->data.p = a->data.p + b->data.p;
	/*Completez campul in care pastrez parintele fiecarui nod*/
	a->parrent = tata;
	b->parrent = tata;
	/*Creez legaturile*/
	tata->fiu_stanga = a;
	tata->fiu_dreapta = b;
	a->codif = NULL;
	b->codif = NULL;
	return tata;
}

void makeCodification(TagHuffmanNode * head)
{
	uint32_t k = 0, i = 0;
	int j;
	TagHuffmanNode *aux;
	int *cod;

	aux = head;
	head->codif = (int *) malloc(head->nivel*sizeof(int));
	cod = (int *)malloc(head->nivel*sizeof(int));
	/*Parcurg arborele de la frunza spre radacina, folosind 
	campul parinte*/
	while(aux->parrent!=NULL)
	{
		/*Daca sunt pe partea stanga, adaug un 0*/
		if(aux->parrent->fiu_stanga == aux)
		{
			head->codif[i] = 0;
			i++;
		}
		/*Daca sunt pe partea dreapta, adaug un 1*/	
		else
		{
			head->codif[i] = 1;
			i++;
		}
		/*Urc in arbore*/
		aux = aux->parrent;
	}
	/*Inversez bitii din codificare	*/
	for(j = i-1; j>=0;j--)
	{
		cod[k] = head->codif[j];
		k++;
	}
	/*Completez codificarea in campul codif din structura*/
	for(j = 0; j<i;j++)
	{
		head->codif[j] = cod[j];
	}
}

/*Functie recursiva care gaseste un nod in arbore dupa caracterul pe 
care il contine*/
TagHuffmanNode* search(TagHuffmanNode*node, unsigned char key)
{
    TagHuffmanNode* found = NULL;

    if(node->fiu_stanga==NULL && node->fiu_dreapta==NULL && node->data.c==key)
    	return node;

    if(node->fiu_stanga!=NULL)
    	found = search(node->fiu_stanga, key);
    if(found!=NULL) 
    	return found;

    if(node->fiu_dreapta!=NULL)
    	found = search(node->fiu_dreapta, key);
    if(found!=NULL)
    	return found;

    return NULL;
}

/*Functie ce parcurge arborele in preordine si face codificarea 
pentru fiecare frunza*/
void printPreorder(TagHuffmanNode* node, int nivel, FILE * fp)
{
	/*Daca nodul este frunza*/
    if (node->fiu_stanga == NULL && node->fiu_dreapta == NULL)
    {
    	node->nivel = nivel;
    	makeCodification(node);
        return;
    }
     /*Apelez recursiv functie pentru subarborele stang */
    if(node->fiu_stanga!=NULL) 
     	printPreorder(node->fiu_stanga,nivel+1,fp); 
 
     /*Apelez recursiv functia pentru subarborele drept*/
     if(node->fiu_dreapta!=NULL) 
     	printPreorder(node->fiu_dreapta,nivel+1,fp);
}    

/*FUNCTII PENTRU COADA DE PRIORITATI*/
//-----------------------------------------------------------------------------
/*Functie ce verifica daca coada este goala*/
int isEmpty()
{
    if( front == NULL)
    	return 1;
    else
   	 	return 0;

}

/*Functie ce insereaza o celula in coada*/
void insert(TagHuffmanNode * item,float item_priority)
{
    struct node *tmp,*p;

    tmp=(struct node *)malloc(sizeof(struct node));
    if(tmp==NULL)
    {
	    printf("Memory not available\n");
	    return;
    }
    tmp->info=item;
    tmp->priority=item_priority;
    /*Daca noua ceula are prioritatea mai mica decat a primului
    element din coada, introduc elementul la inceputul cozii*/
    if( isEmpty() || item_priority <= front->priority )
    {
	    tmp->link=front;
	    front=tmp;
    }
    /*Altfel, parcurg coada si inserez celula pe pozitia corespunzatoare*/
    else
    {
	    p = front;
	    while( p->link!=NULL && p->link->priority <item_priority )
	    p=p->link;
	    tmp->link=p->link;
	    p->link=tmp;
    }
    count++;
}

/*Functie ce sterge un element din coada*/
void del()
{
    struct node *tmp;

    if( isEmpty() )
    {
	    printf("Queue Underflow\n");
	    return;
    }
    else
    {
	    tmp=front;
	    front=front->link;
	    /*Eliberez memoria*/
	    free(tmp);
    }
    count--;
    
}

/*Functie ce afiseaza coada*/
void display()
{
    struct node *ptr;
    ptr=front;
    if( isEmpty() )
    	printf("Queue is empty\n");
    else
    {   
	    printf("Queue is :\n");
	    printf("Priority       Item\n");
	    while(ptr!=NULL)
	    {
		    printf("%c             %f\n",ptr->info->data.c,ptr->info->data.p);
		    ptr=ptr->link;
	    }
    }
}

/*Functie ce returneaza primul element din coada*/
struct node * frontelement()
{
    if (front != NULL) 
        return front;
    else
        return NULL;
}
//-----------------------------------------------------------------------------

/*Functie in care construiesc arborele*/
void buildTree(FILE * fp)
{
	struct node *e1, *e2;

	while(count > 1)
		
	{	
		/*Scot primele 2 elemente din coada*/
		e1 = frontelement();
		del();
		e2 = frontelement();
		del();
		/*Construiesc nodul ce va fi adaugat inpoi in coada si creez arborele*/
		eSum = ins(e1->info,e2->info);
		/*Am pus nodul suma inapoi in coada*/
		insert(eSum,eSum->data.p);

	}
		/*nodul radacina cu probabilitatea 1, nu are parinte*/
		eSum->parrent = NULL; 
		
		BuildLevelOrder(eSum);
		/*Scriu numarul de noduri din arbore in fisierul binar*/
		fwrite(&numar_noduri, sizeof(uint16_t),1,fp);
		printLevelOrder(eSum,fp);
		printPreorder(eSum,0,fp);
}



int main(int argc,char **argv)
{
	TagHuffmanNode cells[50005];
	readLetter ref[500];
	uint32_t i, k = 0, uniq = 0, j, p = 0, q, nr;
	uint32_t countletter = 0;
	FILE * fid, *f, *fp;
	unsigned char x;
	TagHuffmanNode *leaf;
	int codifVec[1000005];
	unsigned char rez[1000005];
	
	/*Comprimare*/
	if (strcmp(argv[1],"-c")==0)
	{
		/*Deschid fisierul pentru scriere*/
		fp = fopen(argv[3],"wb");
		/*Initializez vectorul in care calculez frecventa*/
		for(i = 0; i <= 255; i++)
		{
			ref[i].asc = i;
			ref[i].p = 0;
		}

		countletter=0;
		fid = fopen(argv[2],"r");
		/*Aflu dimensiunea fisierului*/
		uint32_t inpos;
		fseek(fid,0,SEEK_END);
		inpos=ftell(fid);
		fseek(fid,0,SEEK_SET);
		/*Citesc caracter cu caracter textul ce trebuie codificat*/
		uint32_t iter;
		for(iter=1;iter<=inpos;iter++)
		{
			x = fgetc(fid);
			countletter++;
			/*Calculez frecventa*/
			for(i = 0; i <= 255; i++)
				if(x == ref[i].asc)
				{
					ref[i].c = x;
					ref[i].p++;
				}

		}

		/*Scriu numarul de litere in fisierul binar*/
		fwrite(&countletter, sizeof(uint32_t), 1, fp);
		/*Calculez probabilitatile*/
		for(i = 0; i <= 255; i++)
		{
			ref[i].p = (float)ref[i].p/(countletter);
		}
		/*Calculez cate caractere unice se gasesc in text*/
		uniq=0;
		for(i = 0; i <= 255; i++)
			if(ref[i].p)
			{
				uniq++;
			}

		letter str[50000];
		/*Creez un vector ce contine caracterul si probabilitatea*/
		k=0;
		for(i = 0; i <= 255; i++)
			if(ref[i].p)
			{
				str[k].c = ref[i].c;
				str[k].p = ref[i].p;
				k++; 
			}
		/*Creez celulele ce vor fi introduse in coada cu prioritati*/
		for(i = 0; i < uniq; i++)
		{
			cells[i].data = str[i];
			cells[i].fiu_stanga = NULL;
			cells[i].fiu_dreapta = NULL;
		}

		/*Construiesc coada initiala*/
		for(i = 0; i < uniq; i++)
			insert(&cells[i],str[i].p);
		buildTree(fp);
		/*Inchid fisierul de citire*/
		fclose(fid);
		/*Redeschid fisierul pentru a concatena codificarile obtinute*/	
		f = fopen(argv[2],"r");

		leaf=NULL;
		for(iter = 0; iter < countletter; iter++)
		{
			x = fgetc(f);
			/*Caut in arbore caracterul citit*/
			leaf = search(eSum,x);
			if(leaf != NULL)
			{
				/*Adaug codificarea caracterului gasit in vectorul de 
				codificare*/
				for(i = 0; i < leaf->nivel; i++)
				{
					codifVec[p] = leaf->codif[i];
					p++;
				}

			}

		}
		/*Padding de zero*/
		if(p%8!=0)
		{
			q = p%8;
			q = 8-q;
			
			for(j = 0; j < q; j++ )
				codifVec[p+j] = 0;
		}
		p = p+q;
		/*Impart vectorul de codificare si transform din baza 2 in baza 10*/
		for(j = 0; j < p/8; j++)
		{
			nr = 0;
			for(i = 0; i < 8; i++)
			{
				nr = nr*2 + codifVec[8*j+i];
			}
			rez[j] = nr;
		}
		/*Scriu codificarea in fisierul binar*/
		for(j = 0; j < p/8; j++)
		{
			fwrite(&rez[j],sizeof(unsigned char),1,fp);
		}
		/*Inchid fisierul din care an citit*/
		fclose(f);
		/*Inchid fisierul in care am scris*/
		fclose(fp);

}
/*Decomprimare*/
	else
	{
		/*Deschid fisierele*/
		f=fopen(argv[2],"rb");
		fid=fopen(argv[3],"w");

		uint32_t dimensiune_text;
		uint16_t dimensiune_arbore;
		Huffman vec[100005];
		uint32_t y=0;
		int16_t pozitie;
		int aux[10],numarr;
		unsigned char caracter_citit;

		/*Citesc dimensiunea textului si numarul de noduri*/
		fread( &dimensiune_text , sizeof( uint32_t ), 1 , f);
		fread( &dimensiune_arbore , sizeof(uint16_t),1,f);

		/*Citesc nodurile*/
		for(i=0 ; i<dimensiune_arbore ; i++)
		{
			fread(&vec[i],sizeof(Huffman),1,f);
		}

		pozitie=0; 
		while( y<dimensiune_text)
		{
			if( vec[pozitie].left==-1 ) 
			{
				fprintf(fid,"%c",vec[pozitie].c);
				pozitie=0;
				y++;
			}

			if(y>=dimensiune_text) break;

			fread( &caracter_citit , sizeof( unsigned char ), 1 , f);

			numarr = 0;
			for(i = 1; i<=8; i++)
			{
				numarr++;
				aux[numarr] = caracter_citit&1;
				caracter_citit/=2;
			}

			for(i = 8; i ; i--)
			{
				if( aux[i]==1)
					pozitie = vec[pozitie].right;
				else 
					pozitie=vec[pozitie].left;

				if( vec[pozitie].left==-1 ) 
				{
					fprintf(fid,"%c",vec[pozitie].c);
					pozitie = 0;
					y++;
				}
				/*Daca am ajuns la dimensiunea textului*/
				if(y>=dimensiune_text) break;
			}

		}
		/*Inchid fisierele*/
		fclose(f);
		fclose(fid);

	}
	return 0;		
	
}